package com.inacap.diego.certamendos;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements DatosPersonalesFragment.OnFragmentInteractionListener, DatosContactoFragment.OnFragmentInteractionListener, DatosLoginFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragments = null;
        fragments = new DatosPersonalesFragment();

        if (fragments != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragments).commit();
        }




    }

    @Override
    public void onFragmentInteraction(String fragmentName, String action) {
        if(fragmentName.equals("DatosPersonalesFragment")){
            if(action.equals("SIGUIENTE")){
                Fragment fragmento_nuevo = new DatosContactoFragment();

                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.flContent, fragmento_nuevo).commit();
            }
        }
        if(fragmentName.equals("DatosContactoFragment")){
            if(action.equals("SIGUIENTE")){
                Fragment fragmento_nuevo = new DatosLoginFragment();

                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.flContent, fragmento_nuevo).commit();
            }

        }


    }public void onFragmentInteraction(String fragmentName, String action, String Contraseña) {


        if(fragmentName.equals("DatosLoginFragment")){
            if(action.equals("Registrarme")){
                Toast.makeText(getApplicationContext(),"Su contraseña ingresada es: " + Contraseña,Toast.LENGTH_LONG).show();
            }
        }

    }
}
